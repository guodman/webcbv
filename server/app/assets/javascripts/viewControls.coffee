$ ->
  window.imageBuffer = []
  document.onclick = activateClick
  $("#comicImage").load ->
    window.scrollTo(0, 0)
    $(".screenLoading").hide()
  $("#comicImage").error ->
    window.location = "/login"
  updateProgressBar()
  ensurePreload()


needsPage = (position)->
  return position <= window.comicMaxPages && position > 0 && !window.imageBuffer[position]

ensurePreload = ()->
  if (needsPage(window.comicPage))
    bufferImage(window.comicPage)
  else if (needsPage(window.comicPage + 1))
    bufferImage(window.comicPage + 1)
  else if (needsPage(window.comicPage + 2))
    bufferImage(window.comicPage + 2)
  else if (needsPage(window.comicPage - 1))
    bufferImage(window.comicPage - 1)
  else
    console.log("No preloading to do")

bufferImage = (position)->
  console.log("About to preload", position)
  img = new Image()
  img.onload = ()->
    createImageBitmap(img).then( (response) ->
      window.imageBuffer[position] = response
      if (window.comicPage == position)
        displayImage(position)
      ensurePreload()
    )
  img.src = getImageUrl(window.comicPath, position)
  console.log("Loading image from ", getImageUrl(window.comicPath, position))

displayImage = (position)->
  if (window.imageBuffer[position])
    canvas = document.getElementById("comicCanvas")
    context = canvas.getContext("2d")
    img = window.imageBuffer[position]
    console.log("Rendering image: ", position, img, img.width, img.height)
    width = img.width
    height = img.height
    canvas.width = width
    canvas.height = height
    ratio = window.innerWidth / img.width
    $(canvas).css("zoom", ratio)
    $(canvas).css("-moz-transform", ratio)
    $(canvas).css("-moz-transform-origin", "0 0")
    context.drawImage(window.imageBuffer[position], 0, 0, width, height)
    window.scrollTo(0, 0)
    $(".screenLoading").hide()

activateClick = (e)->
  _x = event.clientX + document.body.scrollLeft;
  _y = event.clientY + document.body.scrollTop;
  console.log(_x, _y);
  if (_y < window.innerHeight / 3)
    console.log "close"
    window.location = "/list"
  else if (_y > window.innnerHeight / 3)
    console.log "info"
  else if (_x > window.innerWidth / 2)
    console.log "next"
    $(".screenLoading").show()
    if (window.comicPage < window.comicMaxPages)
      window.comicPage++
    else
      window.location = comicActionUrlNoPage("/viewNext/", window.comicPath)
    displayImage(window.comicPage)
    ensurePreload()
    $.ajax comicActionUrl("/savePlace/", window.comicPath, window.comicPage),
      type: "GET"
  else if (_x < window.innerWidth / 2)
    console.log "previous"
    $(".screenLoading").show()
    window.comicPage--
    if (window.comicPage < 1)
      window.location = comicActionUrlNoPage("/viewPrev/", window.comicPath)
    if (window.comicPage < 1)
      window.comicPage = 1
    displayImage(window.comicPage)
    ensurePreload()
    $.ajax comicActionUrl("/savePlace/", window.comicPath, window.comicPage),
      type: "GET"
  else
    console.log "unknown click"
  updateProgressBar()

updateProgressBar = () ->
  $("#progressFill").css("width", ((comicPage / comicMaxPages) * 100) + "%")

getImageUrl = (comicPath, page) ->
  pth = $('<div />').html(comicPath).text()
  "/img/" + encodeURIComponent(pth) + "?page=#{page}"

comicActionUrl = (action, comicPath, page) ->
  pth = $('<div />').html(comicPath).text()
  action + encodeURIComponent(pth) + "?page=#{page}"

comicActionUrlNoPage = (action, comicPath) ->
  pth = $('<div />').html(comicPath).text()
  action + encodeURIComponent(pth)
