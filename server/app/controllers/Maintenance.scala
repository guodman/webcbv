package controllers

import java.io.File

import models._
import play.api.mvc._

object Maintenance extends Controller {
  def reindex = Action { implicit request =>
    var msg: String = "yes"
    if (request.session.get("user").isEmpty) {
      msg = "no"
      Redirect(routes.UserManagement.login())
    } else {
      indexFolder(new File(Constants.files))
      Ok("Done")
    }
  }

  def indexFolder(path: File): Unit = {
    play.api.Logger.debug("indexing folder " + path.getAbsolutePath)
    path.listFiles.toList
    .filter({ f =>
      !f.isDirectory && (f.getName.endsWith(".cbz") || f.getName.endsWith("zip"))
    })
    .map({ f =>
      Comic.getByPath(f.getAbsolutePath.stripPrefix(models.Constants.files))
    })
    .foreach({ c =>
      var mapping = SlugMapping.getByPath(c.path)
      if (mapping == null) {
        mapping = SlugMapping.generateFromPath(c.path)
        if (mapping != null) {
          mapping.save
        }
      }
    })

    path.listFiles.toArray.toList.filter({ f =>
      f.isDirectory
    })
    .foreach({ dir =>
      indexFolder(dir)
    })
  }
}
