package controllers

import java.io.File

import models._
import play.api.mvc._

object Application extends Controller {
  def index = Action { implicit request =>
    var msg: String = "yes"
    if (request.session.get("user") == None) {
      msg = "no"
      Redirect(routes.UserManagement.login)
    } else {
      Redirect(routes.Application.listBase)
    }
  }

  def list(path: String) = Action { implicit request =>
    val baseDir = new File(Constants.files + java.net.URLDecoder.decode(path, "UTF-8"))
    var user = User.getByUsername(request.session.get("user").getOrElse("Invalid User"))
    if (user != null) {
      if (path contains "..") {
        Redirect(routes.Application.listBase)
      } else if (!baseDir.exists) {
        Redirect(routes.Application.listBase)
      } else {
        val comics = Comic.getComicsInDir(baseDir)
        val totalRead: Int = comics.foldLeft(0)( {(total: Int, next: Comic) =>
          val place = user.getPlace(next)
          if (place == 1) {
            total
          } else {
            total + place
          }
        })
        val totalAvailable: Int = comics.foldLeft(0)( {(total: Int, next: Comic) =>
          total + next.getFullSlug.pageCount
        })
        Ok(views.html.list(baseDir, comics, null, user, totalRead, totalAvailable))
      }
    } else {
      Redirect(routes.UserManagement.login)
    }
  }

  def listBase = Action { implicit request =>
    val baseDir = new File(Constants.files)
    play.api.Logger.debug("checking for user " + request.session.get("user").getOrElse("Invalid User"))
    var user = User.getByUsername(request.session.get("user").getOrElse("Invalid User"))
    if (user != null) {
      val comics = Comic.getComicsInDir(baseDir)
      var currentComic =
        if (user.currentlyReadingPath != null) {
          Comic.getByPath(java.net.URLDecoder.decode(user.currentlyReadingPath, "UTF-8"))
        } else {
          null
        }
      Ok(views.html.list(baseDir, comics, currentComic, user, 0, 0))
    } else {
      Redirect(routes.UserManagement.login)
    }
  }

  def inProgress = Action { implicit request =>
    var user = User.getByUsername(request.session.get("user").getOrElse("Invalid User"))
    if (user != null) {
      val inProgress = user.getInProgress
      Ok(views.html.inprogress(inProgress, user))
    } else {
      Redirect(routes.UserManagement.login)
    }
  }

  def view(path: String, page: Option[Int]) = Action { implicit request =>
    var comic = Comic.getByPath(java.net.URLDecoder.decode(path, "UTF-8"))
    val comicFile = new File(Constants.files + java.net.URLDecoder.decode(path, "UTF-8"))
    var user = User.getByUsername(request.session.get("user").getOrElse("Invalid User"))
    if (user != null) {
      if (path contains "..") {
        Redirect(routes.Application.listBase)
      } else if (!comicFile.exists) {
        Redirect(routes.Application.listBase)
      } else {
        val user = User.getByUsername(request.session.get("user").getOrElse("Invalid User"))
        if (user != null) {
          user.currentlyReadingPath = path
          user.currentlyReadingName = comic.getFileName
          user.save
        }
        val pageCount = comic.pageCount
        Ok(views.html.view(comicFile, page.getOrElse(user.getPlace(comic)), pageCount))
      }
    } else {
      Redirect(routes.UserManagement.login)
    }
  }

  def viewNext(path: String) = Action { implicit request =>
    val current = new File(Constants.files + java.net.URLDecoder.decode(path, "UTF-8"))
    val folder = current.getParentFile
    val candidates = folder.listFiles.toArray.toList.sortWith(sortDirFirst)
    val curIdx = candidates.indexWhere( { c => c.getAbsolutePath == current.getAbsolutePath } )
    var user = User.getByUsername(request.session.get("user").getOrElse("Invalid User"))
    if (user != null) {
      if (curIdx + 1 < candidates.size) {
        val next = candidates(curIdx + 1)
        Redirect(routes.Application.view(Comic.getComicSlug(next.getAbsolutePath), None))
      } else {
        Redirect(routes.Application.listBase)
      }
    } else {
      Unauthorized("Please log in to continue")
    }
  }

  def viewPrev(path: String) = Action { implicit request =>
    val current = new File(Constants.files + java.net.URLDecoder.decode(path, "UTF-8"))
    val folder = current.getParentFile
    val candidates = folder.listFiles.toArray.toList.sortWith(sortDirFirst)
    val curIdx = candidates.indexWhere( { c => c.getAbsolutePath == current.getAbsolutePath } )
    var user = User.getByUsername(request.session.get("user").getOrElse("Invalid User"))
    if (user != null) {
      if (curIdx > 0) {
        val next = candidates(curIdx - 1)
        Redirect(routes.Application.view(Comic.getComicSlug(next.getAbsolutePath), None))
      } else {
        Redirect(routes.Application.listBase)
      }
    } else {
      Unauthorized("Please log in to continue")
    }
  }

  def comicImage(path: String, page: Option[Int]) = Action { implicit request =>
    val user = User.getByUsername(request.session.get("user").getOrElse("Invalid User"))
    if (user != null) {
      val comic = Comic.getByPath(java.net.URLDecoder.decode(path, "UTF-8"))
      Ok(comic.getPageByteArray(page.getOrElse(1))).as("image")
    } else {
      Unauthorized("Please log in to continue")
    }
  }

  def comicPreview(path: String) = Action { implicit request =>
    val user = User.getByUsername(request.session.get("user").getOrElse("Invalid User"))
    if (user != null) {
      val comic = Comic.getByPath(java.net.URLDecoder.decode(path, "UTF-8"))
      Ok(comic.getPreview).as("image")
    } else {
      Unauthorized("Please log in to continue")
    }
  }

  def savePlace(path: String, page: Int) = Action { implicit request =>
    val user = User.getByUsername(request.session.get("user").getOrElse("Invalid User"))
    if (user != null) {
      var comic = Comic.getByPath(java.net.URLDecoder.decode(path, "UTF-8"))
      if (comic == null) {
        comic = new Comic(path)
      }
      if (comic.isValid) {
        user.savePlace(comic, page)
        user.save
      }
      if (user != null) {
        user.currentlyReadingPath = path
        user.currentlyReadingName = comic.getFileName
        user.save
      }
      Ok("done")
    } else {
      Unauthorized("Please log in to continue")
    }
  }

  def markDone(path: String) = Action { implicit request =>
    val user = User.getByUsername(request.session.get("user").getOrElse("Invalid User"))
    if (user != null) {
      var comic = Comic.getByPath(java.net.URLDecoder.decode(path, "UTF-8"))
      if (comic == null) {
        play.api.Logger.debug("Comic not found, using dummy comic")
        comic = new Comic(path)
      }
      if (comic.isValid) {
        user.savePlace(comic, comic.pageCount)
        user.save
      }
      Ok("done")
    } else {
      Unauthorized("Please log in to continue")
    }
  }

  def clearPlace(path: String) = Action { implicit request =>
    val user = User.getByUsername(request.session.get("user").getOrElse("Invalid User"))
    if (user != null) {
      var comic = Comic.getByPath(java.net.URLDecoder.decode(path, "UTF-8"))
      if (comic == null) {
        play.api.Logger.debug("Comic not found, using dummy comic")
        comic = new Comic(path)
      }
      if (comic.isValid) {
        user.clearPlace(comic)
        user.save
      }
      Ok("done")
    } else {
      Unauthorized("Please log in to continue")
    }
  }

  def sortDirFirst(a: java.io.File, b: java.io.File): Boolean = {
    if (a.isDirectory && !b.isDirectory) {
      true
    } else if (b.isDirectory && !a.isDirectory) {
      false
    } else {
      a.getName < b.getName
    }
  }
}
