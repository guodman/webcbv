package controllers

import models._
import play.api.data.Forms._
import play.api.data._
import play.api.mvc._
import play.api.Play.current
import play.api.i18n.Messages.Implicits._

object UserManagement extends Controller {
  def login = Action { implicit request =>
    Ok(views.html.login(loginForm))
  }

  def logout = Action { implicit request =>
    play.api.Logger.debug("Logging Out")
    Redirect(routes.UserManagement.login).withNewSession
  }

  def authenticate = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => Redirect(routes.UserManagement.login).withSession(request.session).flashing("message" -> "Incorrect username or password"),
      {case (username, password) => {
          val user = User.getByUsername(username)
          if (user != null && user.checkPassword(password)) {
            play.api.Logger.debug("logging in as " + user.username)
            Redirect(routes.Application.listBase).withSession(request.session + ("user" -> user.username)).flashing("message" -> ("You have logged in"))
          } else {
            Redirect(routes.UserManagement.login).withSession(request.session).flashing("message" -> "Incorrect  Username or Password")
          }
        }
      }
    )
  }

  def changePassword = Action { implicit request =>
    Ok(views.html.changePassword(passwordForm))
  }

  def submitChangePassword = Action { implicit request =>
    passwordForm.bindFromRequest.fold(
      formWithErrors => Redirect(routes.UserManagement.changePassword).withSession(request.session).flashing("message" -> "Please try again"),
      {case (password, repeat) => {
          val user = User.getByUsername(request.session.get("user").getOrElse("Invalid User"))
          if (user != null) {
            user setPassword password
            user.save
            Redirect(routes.Application.listBase).withSession(request.session + ("user" -> user.username)).flashing("message" -> ("Your password has been changed"))
          } else {
            Redirect(routes.UserManagement.login).withSession(request.session).flashing("message" -> "Please log in, user was null")
          }
        }
      }
    )
  }

  def create = Action { implicit request =>
    val user = new User("doug1")
    user.setPassword("123")
    user.savePlace(new Comic("Metroid/Metroid Manga Vol 1.cbz"), 1)
    user.save
    Redirect(routes.Application.index)
  }

  val loginForm = Form(
    tuple(
      "username" -> nonEmptyText,
      "password" -> nonEmptyText
    )
  )

  val passwordForm = Form(
    tuple(
      "password" -> nonEmptyText,
      "repeat" -> nonEmptyText
    )
  )
}
