package models

import com.mongodb.casbah.MongoClient

object Constants {
  val mdb = MongoClient("10.0.30.212", 27017)
  val db = mdb("webcbv")
  val files = "/opt/comic/"
}
