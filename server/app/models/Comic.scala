package models

import java.awt.geom.AffineTransform
import java.awt.image.AffineTransformOp
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.util
import java.util.zip.ZipEntry
import java.util.zip.ZipException
import java.util.zip.ZipFile

import com.mongodb.casbah.gridfs.GridFS
import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import org.im4java.core.ConvertCmd
import org.im4java.core.IMOperation

class Comic(var path: String) {
  var _zipFile: ZipFile = null
  var _pageCount: Int = -1
  var _sortedNames: Array[ZipEntry] = null

  def jsPath: String = {
    return path.replaceAll("'", "\\\\'")
  }

  def isValid: Boolean = {
    val f = new File(Constants.files + path)
    f.exists
  }

  def slug: String = {
    return Comic.getComicSlug(path)
  }

  def getFullSlug: SlugMapping = {
    SlugMapping.getByPath(path)
  }

  def getFileName: String = {
    val f = new File(Constants.files + path)
    f.getName
  }

  def zipFile: ZipFile = {
    if (_zipFile == null) {
      _zipFile = new ZipFile(Constants.files + path)
    }
    return _zipFile
  }

  def pageCount: Int = {
    play.api.Logger.debug("getting page count for " + path)
    if (_pageCount == -1) {
      try {
        val comicZip = zipFile
        var count = 0
        val entries: util.Enumeration[_ <: ZipEntry] = comicZip.entries
        var img: ZipEntry = null
        while (entries.hasMoreElements) {
          img = entries.nextElement.asInstanceOf[ZipEntry]
          if (!img.isDirectory &&
            (img.getName.toLowerCase.endsWith("jpg")
              || img.getName.toLowerCase.endsWith("jpeg")
              || img.getName.toLowerCase.endsWith("png")
              || img.getName.toLowerCase.endsWith("gif"))) {
            count += 1
          }
        }
        _pageCount = count
      } catch {
        case ze: ZipException => _pageCount = -1
      }
    }
    return _pageCount
  }

  def sortedNames: Array[ZipEntry] = {
    if (_sortedNames == null) {
      val entries: util.Enumeration[_ <: ZipEntry] = zipFile.entries
      var img: ZipEntry = null
      var names: Array[ZipEntry] = Array()
      while (entries.hasMoreElements) {
        img = entries.nextElement.asInstanceOf[ZipEntry]
        if (!img.isDirectory &&
              (img.getName.toLowerCase.endsWith("jpg")
              || img.getName.toLowerCase.endsWith("jpeg")
              || img.getName.toLowerCase.endsWith("png")
              || img.getName.toLowerCase.endsWith("gif"))) {
          // accumulate the names
          names = names :+ img
        }
      }
      // sort the names
      scala.util.Sorting.stableSort(names,
        (a: ZipEntry, b: ZipEntry) => a.getName.toLowerCase < b.getName.toLowerCase)
      _sortedNames = names
    }
    return _sortedNames
  }

  def getPageStream(page: Int): InputStream = {
    val comic = zipFile
    val img = sortedNames(page - 1)
    return comic getInputStream img
  }

  def getPageByteArray(page: Int): Array[Byte] = {
    IOUtils.toByteArray(getPageStream(page))
  }

  def getScaledImage(image: BufferedImage, width: Double): Option[BufferedImage] = {
    try {
      val imageWidth: Double = image.getWidth()
      val imageHeight: Double = image.getHeight()

      val scale: Double = width/imageWidth
      val height: Double = imageHeight * scale
      val scaleTransform: AffineTransform = AffineTransform.getScaleInstance(scale, scale)
      val bilinearScaleOp: AffineTransformOp = new AffineTransformOp(scaleTransform, AffineTransformOp.TYPE_BILINEAR)
      return Option(bilinearScaleOp.filter(image, new BufferedImage(width.asInstanceOf[Int], height.asInstanceOf[Int], image.getType())))
    } catch {
      case e: Exception => {
        play.api.Logger.debug("Unable to resize image: " + path)
        e.printStackTrace()
        return Option[BufferedImage](null)
      }
    }
  }

  def getPreview: Array[Byte] = {
    val grid = GridFS(Constants.db)
    grid.findOne(slug) match {
      case Some(image) => {
        val outStream: ByteArrayOutputStream = new ByteArrayOutputStream()
        image.writeTo(outStream)
        outStream.toByteArray
      }
      case None => {
        val grid = GridFS(Constants.db)
        val preview = generatePreview
        val image = grid.createFile(preview)
        image.filename = slug
        image.save()
        preview
      }
    }
  }

  def generatePreview: Array[Byte] = {
    val (original, convert) = Comic.tmpFilename()
    // write original image to disk
    FileUtils.copyInputStreamToFile(getPageStream(1), new File(original))
    // convert the image
    val cmd = new ConvertCmd()
    cmd.run({
      val op = new IMOperation
      op.addImage(original)
      op.resize(200)
      op.addImage(convert)
      op
    })
    // read the image into the a byte array
    val out = IOUtils.toByteArray(new FileInputStream(new File(convert)))
    // clean up
    new File(original).delete()
    new File(convert).delete()
    return out
  }
}

object Comic {
  def getByPath(path: String): Comic = {
    return new Comic(path)
  }
  
  def getComicSlug(fullPath: String): String = {
    val f = new File(fullPath)
    val rel = f.getPath.stripPrefix(models.Constants.files)
    val encoded = java.net.URLEncoder.encode(rel, "UTF-8")
    return encoded
  }

  def getComicsInDir(path: File): List[Comic] = {
    path.listFiles.toList
    .filter({f =>
      Comic.isComicFile(f)
    })
    .sortWith(sortDirFirst)
    .map({ f =>
      Comic.getByPath(f.getAbsolutePath.stripPrefix(models.Constants.files))
    })
  }

  def isComicFile(f: File): Boolean = {
    return !f.isDirectory && (f.getName.endsWith(".cbz") || f.getName.endsWith("zip"))
  }

  def sortDirFirst(a: java.io.File, b: java.io.File): Boolean = {
    if (a.isDirectory && !b.isDirectory) {
      true
    } else if (b.isDirectory && !a.isDirectory) {
      false
    } else {
      a.getName < b.getName
    }
  }

  def tmpFilename(): (String, String) = {
    val r = new scala.util.Random
    val sb = new StringBuilder
    for (i <- 1 to 2) {
      sb.append(r.nextInt())
    }
    sb.append(".png")
    val rand = sb.toString()
    ("/tmp/webcbv-original-" + rand, "/tmp/webcbv-convert-" + rand)
  }
}
