package models

import java.security.MessageDigest
import scala.collection.JavaConverters._
import com.mongodb.casbah.Imports._
import java.util.UUID

class User(val username: String) {
  var _id: ObjectId = null
  var salt: String = User.newSalt
  var passwordHash: String = User.hash("123", salt)
  var currentlyReadingPath: String = null
  var currentlyReadingName: String = null
  var places: Map[String, Int] = Map()

  def setPassword(password: String) = {
    salt = User.newSalt
    passwordHash = User.hash(password, salt)
  }

  def checkPassword(pass: String): Boolean = {
    return User.hash(pass, salt) == passwordHash
  }

  def savePlace(comic: Comic, place: Int) {
    places = places + (comic.path.replaceAll("""[\.]""", "") -> place)
  }

  def getPlace(comic: Comic): Int = {
    return places.getOrElse(comic.path.replaceAll("""[\.]""", ""), 1)
  }

  def getInProgress: List[Comic] = {
    return places.map({ keyVal =>
      val key = keyVal._1
      val place = keyVal._2
      val mapping = SlugMapping.getByKey(key)
      if (mapping != null) {
        val comic = Comic.getByPath(mapping.path)
        if (comic.isValid && place > 0 && place < mapping.pageCount) {
          comic
        } else {
          play.api.Logger.debug("Not reading comic for key " + key)
          null
        }
      } else {
        play.api.Logger.debug("No mapping for key " + key)
        null
      }
    }).filter({
      _ != null
    }).toList
  }

  def clearPlace(comic: Comic) {
    play.api.Logger.debug("clearing reading status for " + comic.path)
    places = places - comic.path.replaceAll("""[\.]""", "")
  }

  def save = {
    var builder = MongoDBObject.newBuilder
    builder += "_id" -> _id
    builder += "username" -> username
    builder += "passwordHash" -> passwordHash
    builder += "salt" -> salt
    builder += "currentlyReadingPath" -> currentlyReadingPath
    builder += "currentlyReadingName" -> currentlyReadingName
    val dbPlaces: DBObject = places
    builder += "places" -> dbPlaces
    User.dbCollection += builder.result
  }
}

object User {
  val dbCollection = Constants.db("user")
  
  def factory(db: DBObject): User = {
    val r = new User(db.getAsOrElse[String]("username", ""))
    r.passwordHash = db.getAsOrElse[String]("passwordHash", "")
    r.salt = db.getAsOrElse[String]("salt", User.newSalt)
    r.currentlyReadingPath = db.getAsOrElse[String]("currentlyReadingPath", null)
    r.currentlyReadingName = db.getAsOrElse[String]("currentlyReadingName", null)
    var places: Map[String, Int] = Map()
    db.getAsOrElse[BasicDBObject]("places", new BasicDBObject())
        .toMap.asScala
        .foreach( { t => places = places + (t._1.asInstanceOf[String] -> t._2.asInstanceOf[Int]) } )
    r.places = places

    r._id = db.getAs[ObjectId]("_id").get
    return r
  }

  def get(id: ObjectId): User = {
    dbCollection.findOneByID(id).map({ dbobj => factory(dbobj) }).getOrElse[User](null)
  }

  def get(id: Option[String]): Option[User] = {
    if (id.isDefined) {
      Option(get(new ObjectId(id.get)))
    } else {
      None
    }
  }

  def getByUsername(username: String): User = {
    val r = dbCollection.findOne(MongoDBObject("username" -> ("(?i)" + username).r))
    return r.map(factory(_)).headOption.getOrElse[User](null)
  }
  
  def all: List[User] = {
    dbCollection.find.map({ dbobj => factory(dbobj) }).toList
  }

  def hash(pass: String, salt: String): String = {
    val toHash = pass + salt
    val sha1 = MessageDigest.getInstance("SHA1")
    sha1.reset()
    return new sun.misc.BASE64Encoder().encode(sha1.digest(toHash.getBytes))
  }

  def newSalt = UUID.randomUUID.toString
}
