package models

import java.io.File

import com.mongodb.casbah.Imports._

class SlugMapping(val path: String) {
  var _id: ObjectId = null
  def key: String = SlugMapping.pathToKey(path)
  def slug: String = Comic.getComicSlug(path)
  def pageCount: Int = _pages
  var _pages: Int = -1

  def save = {
    var builder = MongoDBObject.newBuilder
    builder += "_id" -> _id
    builder += "path" -> path
    builder += "key" -> key
    builder += "slug" -> slug
    builder += "pages" -> _pages
    SlugMapping.dbCollection += builder.result
  }
}

object SlugMapping {
  val dbCollection = Constants.db("slugMapping")
  
  def factory(db: DBObject): SlugMapping = {
    val r = new SlugMapping(db.getAsOrElse[String]("path", ""))
    r._id = db.getAs[ObjectId]("_id").get

    r._pages = db.getAs[Int]("pages").getOrElse({
      val c = Comic.getByPath(r.path)
      if (c.isValid) {
        c.pageCount
      } else {
        0
      }
    })
    return r
  }

  def getByKey(key: String): SlugMapping = {
    val r = dbCollection.findOne(MongoDBObject("key" -> key))
    return r.map(factory).orNull
  }

  def getByPath(path: String): SlugMapping = {
    val r = dbCollection.findOne(MongoDBObject("path" -> path))
    return r.map(factory).orNull
  }

  def generateFromPath(path: String): SlugMapping = {
    val f = new File(path)
    if (Comic.isComicFile(f)) {
      val c = new SlugMapping(path)
      c._pages = Comic.getByPath(path).pageCount
      return c
    } else {
      return null
    }
  }

  def pathToKey(path: String): String = {
    path.replaceAll("""[\.]""", "")
  }
}
