import sbt.Project.projectToRef

name := "webcbv"

version := "1.0-SNAPSHOT"

lazy val clients = Seq(client)
val scalaVer = "2.11.8"

lazy val server = (project in file("server")).settings(
  scalaVersion := scalaVer,
  scalaJSProjects := clients,
  pipelineStages := Seq(scalaJSProd, gzip),
  libraryDependencies ++= Seq(
    jdbc,
    cache,
    "commons-io" % "commons-io" % "2.5",
    "com.vmunier" %% "play-scalajs-scripts" % "0.4.0", // This must line up with the play version (play-2.4.X -> scripts-0.4.0)
    "org.webjars" % "jquery" % "3.1.0",
    specs2 % Test
  ),
  resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases",
  resolvers += "repo.novus snaps" at "http://repo.novus.com/snapshots/",
  resolvers += "casbah" at "https://oss.sonatype.org/content/groups/scala-tools/",
  resolvers += "commons-io" at "https://mvnrepository.com/artifact/commons-io/commons-io",
  libraryDependencies += "org.mongodb" %% "casbah" % "3.1.1",
  includeFilter in (Assets, LessKeys.less) := "*.less",
  excludeFilter in (Assets, LessKeys.less) := "_*.less"
).enablePlugins(PlayScala)
  .aggregate(clients.map(projectToRef): _*)

lazy val client = (project in file("client")).settings(
  scalaVersion := scalaVer,
  persistLauncher := true,
  persistLauncher in Test := false,
  libraryDependencies ++= Seq(
    "org.scala-js" %%% "scalajs-dom" % "0.8.0",
    "be.doeraene" %%% "scalajs-jquery" % "0.9.0",
    "com.github.japgolly.scalajs-react" %%% "core" % "0.11.1"
  ),
  jsDependencies ++= Seq(
    "org.webjars.bower" % "react" % "15.2.1"
      /        "react-with-addons.js"
      minified "react-with-addons.min.js"
      commonJSName "React",
    "org.webjars.bower" % "react" % "15.2.1"
      /         "react-dom.js"
      minified  "react-dom.min.js"
      dependsOn "react-with-addons.js"
      commonJSName "ReactDOM",
    "org.webjars.bower" % "react" % "15.2.1"
      /         "react-dom-server.js"
      minified  "react-dom-server.min.js"
      dependsOn "react-dom.js"
      commonJSName "ReactDOMServer"
  ),
  mainClass in Compile := Some("org.guodman.webcbv.js.MainJS")
).enablePlugins(ScalaJSPlugin, ScalaJSPlay)

lazy val shared = (crossProject.crossType(CrossType.Pure) in file("shared"))
    .settings(scalaVersion := scalaVer)
    .jsConfigure(_ enablePlugins ScalaJSPlay)

lazy val sharedJvm = shared.jvm
lazy val sharedJs = shared.js

// loads the Play project at sbt startup
onLoad in Global := (Command.process("project server", _: State)) compose (onLoad in Global).value
