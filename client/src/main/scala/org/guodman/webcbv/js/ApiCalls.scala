package org.guodman.webcbv.js

import org.scalajs.dom
import org.scalajs.dom.ext.Ajax

import scala.concurrent.ExecutionContext
import scala.scalajs.js.URIUtils
import scala.scalajs.js.annotation.JSExport

@JSExport
object ApiCalls {
  implicit val ec: ExecutionContext = scala.scalajs.concurrent.JSExecutionContext.queue

  @JSExport
  def resetReadingStatus(comicPath: scalajs.js.Any): Unit = {
    Ajax.delete(s"/clearPlace/${URIUtils.encodeURIComponent(comicPath.toString)}")
      .onComplete({ result =>
        dom.window.location.reload(true)
      })
  }

  @JSExport
  def markDone(comicPath: scalajs.js.Any): Unit = {
    Ajax.post(s"/markDone/${URIUtils.encodeURIComponent(comicPath.toString)}")
      .onComplete({ result =>
        dom.window.location.reload(true)
      })
  }
}
