package org.guodman.webcbv.js.react

import japgolly.scalajs.react.ReactComponentB
import japgolly.scalajs.react.vdom.prefix_<^._
import org.guodman.webcbv.js.model.Comic

object ComicBlock {
  val ComicBlock = ReactComponentB[Comic]("ComicBlock")
    .render_P(comic =>
      <.span(^.`class` := "comicBlock",
        <.div(
          <.a(^.href := s"/view/${comic.slug}",
            <.div(^.`class` := "comicBlockImage",
              <.img(^.src := comic.imagePreview, ^.width := 100.px)
            ),
            <.div(^.`class` := "comicBlockTitle",
              comic.title
            )
          )
        ),
        <.div(^.position.relative, ^.width := 100.pct, ^.marginTop := 15.px,
          <.div(
            ^.position.absolute,
            ^.top := 0,
            ^.left := 0,
            ^.width := (comic.readRatio * 100).pct,
            ^.height := 100.pct,
            ^.backgroundColor := "#ccc"
          ),
          <.div(^.position.absolute, ^.top := 0, ^.left := 0, ^.width := 100.pct,
            s"${comic.currentPage} / ${comic.pageCount}"
          ),
          <.div("_")
        ),
        <.div(
          <.a(^.href := s"javascript:org.guodman.webcbv.js.ApiCalls().resetReadingStatus('${comic.jsPath}');", "Reset"),
          " :: ",
          <.a(^.href := s"javascript:org.guodman.webcbv.js.ApiCalls().markDone('${comic.jsPath}');", "Done")
        )
      )
    )
    .build

  val ComicList = ReactComponentB[List[Comic]]("ComicList")
    .render_P(names =>
      <.div(names.map(ComicBlock(_)))
    ).build
}
