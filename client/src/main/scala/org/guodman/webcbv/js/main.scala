package org.guodman.webcbv.js

import japgolly.scalajs.react.ReactDOM
import org.guodman.webcbv.js.model.Comic
import org.guodman.webcbv.js.model.ComicList
import org.guodman.webcbv.js.react.ComicBlock
import org.scalajs.dom
import org.scalajs.jquery.JQuery
import org.scalajs.jquery.jQuery

import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport

@JSExport
object MainJS {
  @JSExport
  def main(): Unit = {
    import JQueryEqualHeight.jq2equalHeight
    import JQueryExtra.jq2jqExtra
    jQuery(dom.document).ready({ () =>
      jQuery(".comicBlock").responsiveEqualHeightGrid()
    })
    jQuery(dom.window).load({ () =>
      jQuery(".comicBlockImage").equalHeight()
      jQuery(".comicBlockTitle").equalHeight()
    })
  }

  @JSExport
  def loadComicList(comics: ComicList): Unit = {
    val mounted = ReactDOM.render(ComicBlock.ComicList(comics.comics.sortBy(_.title)), dom.document.getElementById("comicList"))
  }

  @JSExport
  def loadCurComic(comics: ComicList): Unit = {
    val mounted = ReactDOM.render(ComicBlock.ComicList(comics.comics.sortBy(_.title)), dom.document.getElementById("currentComicHolder"))
  }
}

@scalajs.js.native
trait JQueryExtra extends JQuery {
  def load(handler: js.Any): Unit = js.native
}

object JQueryExtra {
  implicit def jq2jqExtra(jq: JQuery): JQueryExtra = {
    jq.asInstanceOf[JQueryExtra]
  }
}

@scalajs.js.native
trait JQueryEqualHeight extends JQuery {
  def responsiveEqualHeightGrid(): Unit = js.native
  def equalHeight(): Unit = js.native
}

object JQueryEqualHeight {
  implicit def jq2equalHeight(jq: JQuery): JQueryEqualHeight = {
    jq.asInstanceOf[JQueryEqualHeight]
  }
}
