package org.guodman.webcbv.js.model

import scala.scalajs.js.annotation.JSExport

@JSExport
case class Comic(
                  title: String,
                  currentPage: Int,
                  pageCount: Int,
                  jsPath: String,
                  slug: String
                ) {
  def imagePreview: String = s"/preview/${slug}"

  def readRatio: Float = {
    val cur: Float = if (currentPage == 1) { 0 } else { currentPage }
    cur / pageCount
  }
}

@JSExport
class ComicList {
  var comics: List[Comic] = List()

  @JSExport
  def add(c: Comic): Unit = {
    comics = c :: comics
  }
}
